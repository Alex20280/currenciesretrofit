package layouts.sourceit.com.curenciesretrofit;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity {

    TextView cExchange;
    TextView bankName;
    TextView usdTextview;
    TextView eurTextview;
    TextView rubTextview;
    TextView askTextview;
    TextView bidTextview;
    TextView usdAsk;
    TextView askEur;
    TextView rubAsk;
    TextView usdBid;
    TextView eurBid;
    TextView rubBid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main); //*//

        cExchange = (TextView) findViewById(R.id.cExchange);
        bankName = (TextView) findViewById(R.id.bankName);
        usdTextview = (TextView) findViewById(R.id.usdTextview);
        eurTextview = (TextView) findViewById(R.id.eurTextview);
        rubTextview = (TextView) findViewById(R.id.rubTextview);
        askTextview = (TextView) findViewById(R.id.askTextview);
        bidTextview = (TextView) findViewById(R.id.bidTextview);
        usdAsk = (TextView) findViewById(R.id.usdAsk);
        askEur = (TextView) findViewById(R.id.askEur);
        rubAsk = (TextView) findViewById(R.id.rubAsk);
        usdBid = (TextView) findViewById(R.id.usdBid);
        eurBid = (TextView) findViewById(R.id.eurBid);
        rubBid = (TextView) findViewById(R.id.rubBid);

        Retrofit.getBanks(new Callback<Resource>() {
            @Override
            public void success(Resource resource, Response response) {
                bankName.setText(resource.organizations.get(2).title);

                usdAsk.setText(resource.currencies.get(2).USD.ask);
                askEur.setText(resource.currencies.get(2).EUR.ask);
                rubAsk.setText(resource.currencies.get(2).RUB.ask);

                usdBid.setText(resource.currencies.get(2).USD.bid);
                eurBid.setText(resource.currencies.get(2).EUR.bid);
                rubBid.setText(resource.currencies.get(2).RUB.bid);
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(MainActivity.this, "Error", Toast.LENGTH_LONG).show();
            }
        });
    }
}
