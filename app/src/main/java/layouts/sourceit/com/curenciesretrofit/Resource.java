package layouts.sourceit.com.curenciesretrofit;

import java.util.List;

public class Resource {

    List<Organization> organizations;
    List<Currencies> currencies;

    public static class Organization {
        String title;
    }

    public static class Currencies {
        SingleCurrency USD;
        SingleCurrency EUR;
        SingleCurrency RUB;
    }

    public static class SingleCurrency {
        String ask;
        String bid;
    }

}
