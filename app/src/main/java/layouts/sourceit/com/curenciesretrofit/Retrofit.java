package layouts.sourceit.com.curenciesretrofit;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;

public class Retrofit {
    public static final String ENDPOINT = "http://resources.finance.ua";
    private static ApiInterface apiInterface;

    static {
        initialize();
    }

    interface ApiInterface{
        @GET("/ru/public/currency-cash.json")
        void getBanks(Callback<Resource> callback);
    }

    private static void initialize() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        apiInterface = restAdapter.create(ApiInterface.class);
    }

    public static void getBanks(Callback<Resource> callback) {
        apiInterface.getBanks(callback);
    }
}
